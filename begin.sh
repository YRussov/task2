#!/bin/bash
mysql -uroot -pcloudera --force < scripts/create_user.sql;
python3 src/create_db.py;
python3 src/fill_db.py;
source scripts/import_db.sh;
hadoop fs -ls /user/cloudera/unix_syslog_data/output;
