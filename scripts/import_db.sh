#!/bin/bash
hadoop fs -rm -R unix_syslog_data
sqoop import --connect jdbc:mysql://localhost/task2_db --username test_user --password test_user --table unix_syslog_data --num-mappers 1;
