#!/usr/bin/env python3
"""Module defines variables to connect to database"""
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


db_type = 'mysql'
db_host = 'localhost'
db_name = 'task2_db'#'test_user_db'
username = 'test_user'
password = 'test_user'
url = '%s://%s:%s@%s/%s' % (db_type, username, password, db_host, db_name)
engine = create_engine(url, echo=False)
Session = sessionmaker(bind=engine)

"""Some examples of engine for different databases
#PostgreSQL
#default
engine=create_engine('postgresql://scott:tiger@localhost/mydatabase')
#psycopg2
engine=create_engine('postgresql+psycopg2://scott:tiger@localhost/mydatabase')
#pg8000
engine=create_engine('postgresql+pg8000://scott:tiger@localhost/mydatabase')
#default
engine=create_engine('mysql://scott:tiger@localhost/foo')
#MySQL
#mysql-python
engine=create_engine('mysql+mysqldb://scott:tiger@localhost/foo')
#MySQL-connector-python
engine=create_engine('mysql+mysqlconnector://scott:tiger@localhost/foo')
#Oracle
#OurSQL
engine=create_engine('mysql+oursql://scott:tiger@localhost/foo')
engine=create_engine('oracle://scott:tiger@127.0.0.1:1521/sidname')
engine=create_engine('oracle+cx_oracle://scott:tiger@tnsname')
#MSSQL
#pyodbc
engine=create_engine('mssql+pyodbc://scott:tiger@mydsn')
#pymssql
engine=create_engine('mssql+pymssql://scott:tiger@hostname:port/dbname')
#SQLite
#sqlite://<nohostname>/<path>
#where <path> is relative:
engine=create_engine('sqlite:///foo.db')
Unix/Mac - 4 initial slashes in total
engine=create_engine('sqlite:////absolute/path/to/foo.db')
#Windows
engine=create_engine('sqlite:///C:\\path\\to\\foo.db')
#Windows alternative using raw string
engine=create_engine(r'sqlite:///C:\path\to\foo.db')"""

