#!/usr/bin/env python3
"""FILL DATABASE"""
from calendar import monthrange
from create_db import Person
from datetime import date, timedelta
from engine import Session
from random import randint
import random

if __name__ == '__main__':
    
    # message types: debug(7), info(6), notice(5), warning/warn(4), error(3), crit(2), alert(1), emerg/panic(0)
    #type_list = dict(debug=7,info=6,notice=5,warning=4,error=3,crit=2,alert=1,emerg=0)
    
    facility_level_list = ['auth','authpriv','cron','daemon','ftp','kern','lpr','mail','mark','news','security',
                           'syslog', 'user', 'uucp']
    priority_level_list = ['debug','info','notice','warning','error','crit','alert','emerg']
    
    # число записей в таблице
    record_num = (10**3)*2
    
    session = Session()
    
    for index in range(10**3,10**3 + record_num):
        
        fl = random.choice(facility_level_list)
        pl = random.choice(priority_level_list)
        hour = random.randint(1,24)
        if pl == 'debug':
            pt = 7
        elif pl == 'info':
            pt = 6
        elif pl == 'notice':
            pt = 5
        elif pl == 'warning':
            pt = 4
        elif pl == 'error':
            pt = 3
        elif pl == 'crit':
            pt = 2
        elif pl == 'alert':
            pt = 1
        elif pl == 'emerg':
            pt = 0 
            
        msg = 'syslog message:{} {} and priority type: {}'.format(fl, pl, pt)
        
        person = Person(priority_level=pl,priority_type=pt,facility_level=fl,message=msg,hour=hour)
        session.add(person)
        
        #if index % record_num == 0:
            #session.commit()
    
    session.commit()

