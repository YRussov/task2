from pyspark import SparkContext
from pyspark.sql import SQLContext, Row
import pyspark.sql.functions as func
from pyspark.sql.types import DecimalType
import sys
import os


class SPRK(object):
    def __init__(self, input=sys.stdin):
        self.input = input
        self.sc = SparkContext(appName='Spark SQL')
        self.spark = SQLContext(self.sc)
        self.lines = None
        self.rows = None
        self.schema_name = None
        self.df = None

    def read(self):
        """Read text file with data"""
        self.lines = self.sc.textFile(self.input)

    def parse(self, col_idx={}, sep=','):
        """Parse text lines to tokens"""
        tokens = self.lines.map(lambda x: x.strip().split(sep))
        self.rows = tokens.map(
            lambda x: Row(
                **{key: x[value] for (key, value) in col_idx.items()}
            )
        )

    def createDataFrame(self, schema_name='schema'):
        """Create DataFrame and register schema"""
        self.schema_name = schema_name
        self.df = self.spark.createDataFrame(self.rows)
        self.df.registerTempTable(schema_name)

    def read_create(self, col_idx={}, sep=','):
        self.read()
        self.parse(col_idx=col_idx, sep=sep)
        self.createDataFrame()


if __name__ == '__main__':
    
    path_from = '/user/cloudera/unix_syslog_data/'
    fname = 'part-m-[0-9]*'
    full_path = os.path.join(path_from, fname)
    
    sprk = SPRK(input=full_path)
    
    col_idx = {
        'priority_level':1,
        'priority_type':2,
        'facility_level':3,
        'message':4,
        'hour':5
    }
    
    sprk.read_create(col_idx=col_idx, sep=',')
    
    #
    
    # выбор полей
    df = sprk.df.select(
        sprk.df['priority_type'],
        sprk.df['hour'],
        sprk.df['message']
    )
    
    # группировка
    df = df.groupBy('priority_type', 'hour').agg(
        func.count('message').cast(DecimalType(10, 2)).alias('num_of_messages')
    ).orderBy(func.asc('priority_type'))
    
    # выбор результата
    df = df.select(
        df['hour'],
        df['priority_type'],
        df['num_of_messages']
    ).orderBy(func.asc('hour'))
    
    path_to = os.path.join(path_from, 'output')
    
    df \
        .coalesce(1) \
        .write \
        .save(path_to, format='json')
    df.show()
    sprk.sc.stop()

