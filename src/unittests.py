"""unittests for SPRK"""
import unittest
from spark_processing import SPRK
import os
import pandas as pd
from pandas.util.testing import assert_frame_equal


class TestDataFrame(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        """Common Var's"""
        super(TestDataFrame, self).__init__(*args, **kwargs)
        self.string = 'Test string 1\nTest string 2'
        self.path = os.path.dirname(os.path.abspath(__file__))
        self.fname = 'test.txt'
        self.full_path = os.path.join(self.path, self.fname)

    def test_read(self):
        """Read from text file"""
        try:
            with open(self.full_path, mode='wt', encoding='utf-8') as f:
                f.write(self.string)
            sprk = SPRK(input=self.full_path)
            sprk.read()
            self.assertEqual(
                sprk.lines.collect(),
                self.string.split('\n')
            )
        finally:
            os.remove(self.full_path)

    def test_parse(self):
        """Parse text lines"""
        try:
            with open(self.full_path, mode='wt', encoding='utf-8') as f:
                f.write(self.string)
            sprk = SPRK(input=self.full_path)
            sprk.read()
            idxs = list(range(len(self.string.split('\n')[0].split(' '))))
            cols = ['col' + str(i) for i in idxs]
            sprk.parse(sep=' ', col_idx=dict(zip(cols, idxs)))
            self.assertEqual(
                sorted(list(sprk.rows.collect()[0].asDict().values())),
                sorted(self.string.split('\n')[0].split(' '))
            )
        finally:
            os.remove(self.full_path)

    def test_createDataFrame(self):
        """Create DataFrame and Register schema"""
        try:
            with open(self.full_path, mode='wt', encoding='utf-8') as f:
                f.write(self.string)
            sprk = SPRK(input=self.full_path)
            sprk.read()
            data = list(map(lambda x: x.split(' '), self.string.split('\n')))
            idxs = list(range(len(data[0])))
            cols = ['col' + str(i) for i in idxs]
            sprk.parse(sep=' ', col_idx=dict(zip(cols, idxs)))
            sprk.createDataFrame()
            assert_frame_equal(
                sprk.df.toPandas(),
                pd.DataFrame(data=data, columns=cols)
            )
        finally:
            os.remove(self.full_path)


if __name__ == '__main__':
    unittest.main()
    sc.stop()
